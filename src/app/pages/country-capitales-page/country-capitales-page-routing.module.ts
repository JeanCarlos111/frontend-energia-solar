import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CountryCapitalesPagePage } from './country-capitales-page.page';

const routes: Routes = [
  {
    path: '',
    component: CountryCapitalesPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CountryCapitalesPagePageRoutingModule {}
