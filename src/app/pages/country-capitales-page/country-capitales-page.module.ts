import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterCapitalesPipe } from 'src/app/pipes/capitales/filter-capitales.pipe';

import { IonicModule } from '@ionic/angular';

import { CountryCapitalesPagePageRoutingModule } from './country-capitales-page-routing.module';

import { CountryCapitalesPagePage } from './country-capitales-page.page';
import { CountryCapitalesComponent } from 'src/app/components/country-capitales/country-capitales.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CountryCapitalesPagePageRoutingModule
  ],
  declarations: [CountryCapitalesPagePage, CountryCapitalesComponent, FilterCapitalesPipe]
})
export class CountryCapitalesPagePageModule {}
