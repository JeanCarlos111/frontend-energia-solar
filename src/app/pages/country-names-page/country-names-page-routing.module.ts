import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CountryNamesPagePage } from './country-names-page.page';

const routes: Routes = [
  {
    path: '',
    component: CountryNamesPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CountryNamesPagePageRoutingModule {}
