import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterNamesPipe } from 'src/app/pipes/names/filter-names.pipe';

import { IonicModule } from '@ionic/angular';

import { CountryNamesPagePageRoutingModule } from './country-names-page-routing.module';

import { CountryNamesPagePage } from './country-names-page.page';
import { CountryNamesComponent } from 'src/app/components/country-names/country-names.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CountryNamesPagePageRoutingModule
  ],
  declarations: [CountryNamesPagePage, CountryNamesComponent, FilterNamesPipe]
})
export class CountryNamesPagePageModule {}
