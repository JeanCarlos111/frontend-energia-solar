import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FilterPaginationPipe } from 'src/app/pipes/pagination/filter-pagination.pipe';

import { IonicModule } from '@ionic/angular';

import { CountryPageRoutingModule } from './country-routing.module';

import { CountryPage } from './country.page';
import { CountryDataComponent } from 'src/app/components/country-data/country-data.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CountryPageRoutingModule,
  ],
  declarations: [CountryPage, CountryDataComponent, FilterPaginationPipe]
})
export class CountryPageModule {}
