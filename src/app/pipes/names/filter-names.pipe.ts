import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterNames',
})
export class FilterNamesPipe implements PipeTransform {
  transform(names: any[], page: number = 0, search: string = ''): any[] {
    if (search.length === 0) return names.slice(page, page + 9);

    const filteredCountrys = names.filter((c) => c.name.includes(search));
    return filteredCountrys.slice(page, page + 9);
  }
}
