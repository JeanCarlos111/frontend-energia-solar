import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPagination',
})
export class FilterPaginationPipe implements PipeTransform {
  transform(countrys: any[], page: number = 0, search: string = ''): any[] {
    if (search.length === 0) return countrys.slice(page, page + 9);

    const filteredCountrys = countrys.filter((c) => c.name.includes(search));
    return filteredCountrys.slice(page, page + 9);
  }
}
