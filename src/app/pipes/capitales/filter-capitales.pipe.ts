import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterCapitales',
})
export class FilterCapitalesPipe implements PipeTransform {
  transform(capital: any[], page: number = 0, search: string = ''): any[] {
    if (search.length === 0) return capital.slice(page, page + 9);

    const filteredCountrys = capital.filter((c) => c.capital.includes(search));
    return filteredCountrys.slice(page, page + 9);
  }
}
