import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  api = 'https://restcountries.eu/rest/v2/all';

  constructor(private http: HttpClient) {}

  getCountrys() {
    return this.http.get(this.api);
  }
}
