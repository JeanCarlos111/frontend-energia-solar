import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiCapitalesService {
  apiCapitales = 'https://restcountries.eu/rest/v2/all?fields=capital';
  constructor(private http: HttpClient) {}

  getCountrysCapital() {
    return this.http.get(this.apiCapitales);
  }
}
