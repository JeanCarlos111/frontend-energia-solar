import { TestBed } from '@angular/core/testing';

import { ApiCapitalesService } from './api-capitales.service';

describe('ApiCapitalesService', () => {
  let service: ApiCapitalesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiCapitalesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
