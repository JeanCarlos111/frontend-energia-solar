import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiNamesService {
  apiNames = 'https://restcountries.eu/rest/v2/all?fields=name';

  constructor(private http: HttpClient) {}

  getCountryNames() {
    return this.http.get(this.apiNames);
  }
}
