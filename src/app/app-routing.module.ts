import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'country',
    loadChildren: () => import('./pages/country/country.module').then( m => m.CountryPageModule)
  },

 
  {
    path: 'country-names-page',
    loadChildren: () => import('./pages/country-names-page/country-names-page.module').then( m => m.CountryNamesPagePageModule)
  },
  
  {
    path: 'country-capitales-page',
    loadChildren: () => import('./pages/country-capitales-page/country-capitales-page.module').then( m => m.CountryCapitalesPagePageModule)
  },

  {
    path: '',
    redirectTo: 'country',
    pathMatch: 'full'
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
