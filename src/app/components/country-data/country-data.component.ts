import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-country-data',
  templateUrl: './country-data.component.html',
  styleUrls: ['./country-data.component.scss'],
})
export class CountryDataComponent implements OnInit {
  public countrys: any = [];
  public pages: number = 0;
  public search: string = '';

  constructor(
    private countryDataService: ApiService,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.getCountrysAll();
  }

  getCountrysAll() {
    this.countryDataService.getCountrys().subscribe(
      (res) => {
        this.countrys = res;
      },
      (error) => console.log(error)
    );
  }

  nextPage() {
    this.pages += 9;
  }

  previousPage() {
    if (this.pages > 0) this.pages -= 9;
  }

  OnSearchCountrys(search: string) {
    this.pages = 0;
    this.search = search;
  }
}
