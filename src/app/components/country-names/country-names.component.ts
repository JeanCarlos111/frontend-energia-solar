import { Component, OnInit } from '@angular/core';
import { ApiNamesService } from 'src/app/services/api-names/api-names.service';

@Component({
  selector: 'app-country-names',
  templateUrl: './country-names.component.html',
  styleUrls: ['./country-names.component.scss'],
})
export class CountryNamesComponent implements OnInit {
  public names: any = [];
  public pages: number = 0;
  public search: string = '';

  constructor(private getNamesCountryService: ApiNamesService) {}

  ngOnInit() {
    this.getNames();
  }

  getNames() {
    this.getNamesCountryService.getCountryNames().subscribe(
      (res) => {
        this.names = res;
      },
      (error) => console.log(error)
    );
  }

  nextPage() {
    this.pages += 9;
  }

  previousPage() {
    if (this.pages > 0) this.pages -= 9;
  }

  OnSearchCountrys(search: string) {
    this.pages = 0;
    this.search = search;
  }
}
