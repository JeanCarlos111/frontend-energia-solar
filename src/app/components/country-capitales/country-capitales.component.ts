import { Component, OnInit } from '@angular/core';
import { ApiCapitalesService } from 'src/app/services/api-capitales/api-capitales.service';

@Component({
  selector: 'app-country-capitales',
  templateUrl: './country-capitales.component.html',
  styleUrls: ['./country-capitales.component.scss'],
})
export class CountryCapitalesComponent implements OnInit {
  public capitales: any = [];
  public pages: number = 0;
  public search: string = '';

  constructor(private getCapitalesService: ApiCapitalesService) {}

  getCapitales() {
    return this.getCapitalesService.getCountrysCapital().subscribe(
      (res) => {
        this.capitales = res;
      },
      (error) => console.log(error)
    );
  }

  ngOnInit() {
    this.getCapitales();
  }

  nextPage() {
    this.pages += 9;
  }

  previousPage() {
    if (this.pages > 0) this.pages -= 9;
  }

  OnSearchCountrys(search: string) {
    this.pages = 0;
    this.search = search;
  }
}
