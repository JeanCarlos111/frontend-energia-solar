import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'frontend-es-ws',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
